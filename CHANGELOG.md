# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.0

- minor: Fix for fail when waiting and better error ouput

## 0.3.0

- minor: Added support for failing by CVSS2_SCORE and retrieving all findings

## 0.2.0

- minor: Added support for handling unsupported docker os images

## 0.1.0

- minor: Initial Implementation

