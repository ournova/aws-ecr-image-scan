FROM python:3.8-slim

RUN apt-get update \
    && apt-get install --no-install-recommends -y docker=1.5-2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# install requirements
COPY requirements.txt /usr/bin
WORKDIR /usr/bin
RUN pip install -r requirements.txt

# copy the pipe source code
COPY pipe /usr/bin/
COPY pipe.yml /usr/bin

ENTRYPOINT ["python3", "/usr/bin/pipe.py"]