from functools import partial
from collections import Counter
import sys
import time
from typing import Dict

import boto3
from botocore.exceptions import ClientError
from pprint import pformat

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'IMAGE_NAME': {'type': 'string', 'required': True},
    'IGNORE_FINDINGS': {'type': 'list', 'required': False, 'default': []},
    'TAG': {'type': 'string', 'required': False, 'nullable': True, 'default': 'latest'},
    'FAIL_ON': {'type': 'string', 'required': False, 'default': 'MEDIUM HIGH CRITICAL'},
    'MAX_CVSS_SCORE': {'type': 'number', 'required': False, 'nullable': True, 'default': None},
    'TIMEOUT': {'type': 'integer', 'required': False, 'default': 60},
    'IGNORE_UNSUPPORTED': {'type': 'boolean', 'required': False, 'default': False},
    'ALL_FINDINGS': {'type': 'boolean', 'required': False, 'default': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
}


def _compare_cvss(finding: Dict, max_cvss: float = 0):
    """ Compares a findings CVSS2 score to a maximum allowed value """

    for attr in finding.get('attributes', []):
        if attr.get('key') == 'CVSS2_SCORE':
            return float(attr.get('value')) >= max_cvss
    return False


class ECRScan(Pipe):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_client(self):
        try:
            return boto3.client('ecr', region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as err:
            self.fail("Failed to create boto3 client.\n" + str(err))

    def exit(self, exit_code=0):
        image_name = self.get_variable('IMAGE_NAME')
        tag = self.get_variable('TAG')
        console = f"https://{self.get_variable('AWS_DEFAULT_REGION')}.console.aws.amazon.com/ecr/repositories/{image_name}/"
        self.success(
            message=f"Successfully scanned {image_name}:{tag}.\n Available : {console}"
            )
        sys.exit(exit_code)

    def start_scan(self, wait=False):
        image_name = self.get_variable('IMAGE_NAME')
        tag = self.get_variable('TAG')
        client = self.get_client()
        try:
            response = client.start_image_scan(repositoryName=image_name, imageId={'imageTag': tag})
        except ClientError as e:
            error = e.response.get('Error')
            code = error.get('Code')
            msg = error.get('Message')
            self.log_info(code)
            self.log_info(msg)
            return {'error': code}

        image_scan_status = response.get('imageScanStatus')
        status = image_scan_status.get('status')
        if status == 'FAILED':
            self.fail('Failed start image scan')

        return response

    def _check_failed(self, response: dict):
        status = response.get('imageScanStatus').get('status')
        description = response.get('imageScanStatus').get('description')
        ignore_unsupported = self.get_variable('IGNORE_UNSUPPORTED')
        if status == 'FAILED':
            if 'UnsupportedImageError' in description and ignore_unsupported:
                self.log_warning(f"!!!Unsupported Image being ignored!!!")
                self.log_warning(f"{description}")
                self.exit()
            else:
                self.fail(f"AWS Scan has failed:\n{description}")

    def _wait_for_scan(self):
        client = self.get_client()
        image_name = self.get_variable('IMAGE_NAME')
        tag = self.get_variable('TAG')
        timeout = self.get_variable('TIMEOUT')
        timeout = timeout if timeout > 0 else 30*60*60  # if value is 0 set a max of about 30 minutes

        self.log_info("Waiting for scan to complete")
        start_time = int(time.time())

        for i in range(timeout):
            time.sleep(1)
            response = client.describe_image_scan_findings(repositoryName=image_name, imageId={'imageTag': tag})
            status = response.get('imageScanStatus').get('status')
            if status == 'COMPLETE':
                self.log_info('Scan has completed')
                break

            self._check_failed(response)

            self.log_info((i+1)*'.')
        else:
            self.fail("Timed out waiting for scan to complete")

        end_time = int(time.time())

        self.log_info(f'Completed scan in {end_time-start_time}s')

        return response

    def get_findings(self):
        image_name = self.get_variable('IMAGE_NAME')
        tag = self.get_variable('TAG')
        client = self.get_client()
        try:
            response = client.describe_image_scan_findings(repositoryName=image_name, imageId={'imageTag': tag})
        except ClientError as e:
            error = e.response.get('Error')
            code = error.get('Code')
            msg = error.get('Message')
            self.log_info(code)
            self.log_info(msg)
            return {'error': code}

        status = response.get('imageScanStatus').get('status')

        self._check_failed(response)

        if status == 'IN_PROGRESS':
            response = self._wait_for_scan()

        return response

    def _get_next_scan_result(self, token: str):
        image_name = self.get_variable('IMAGE_NAME')
        tag = self.get_variable('TAG')
        client = self.get_client()
        return client.describe_image_scan_findings(repositoryName=image_name, imageId={'imageTag': tag}, nextToken=token)

    def process_findings(self, scan_result: dict):
        image_scan_findings: dict = scan_result.get('imageScanFindings')
        findings: list = image_scan_findings.get('findings').copy()

        if self.get_variable('ALL_FINDINGS'):
            # Get all the results if there are more
            next_token = scan_result.get('nextToken')
            while next_token is not None:
                next_scan = self._get_next_scan_result(next_token)
                next_findings = next_scan.get('imageScanFindings').get('findings')
                findings.extend(next_findings)
                next_token = next_scan.get('nextToken')

        fail_on = set(self.get_variable('FAIL_ON').split())
        ignore_findings = set((f for f in self.get_variable('IGNORE_FINDINGS')))
        max_cvss = self.get_variable('MAX_CVSS_SCORE')

        filtered_findings = findings

        if max_cvss is not None:
            filtered_findings = filter(partial(_compare_cvss, max_cvss=max_cvss), filtered_findings)
        else:
            filtered_findings = filter(lambda v: v.get('severity') in fail_on, filtered_findings)

        if ignore_findings:
            filtered_findings = filter(lambda v: v.get('name', True) not in ignore_findings, filtered_findings)

        filtered_findings = list(filtered_findings)

        if filtered_findings:
            # Print per filter type message
            if max_cvss:
                self.log_error(f'Found {len(filtered_findings)} of vulnerabilites above CVSS2 {max_cvss}')
            else:
                results = Counter(map(lambda v: v.get('severity', 'UNDEFINED'), filtered_findings))
                for severity, count in results.items():
                    self.log_error(f'Found {count} of {severity}')
            # Print findings
            for finding in filtered_findings:
                self.log_error(pformat(finding, indent=2))
            self.fail("Scan found vulnerabilities in the image")
        else:
            if max_cvss:
                self.log_info(f"No scan results matching CVSS2 score above {max_cvss}")
            else:
                self.log_info(f"No scan results matching filter: {self.get_variable('FAIL_ON')}")

    def run(self):
        super().run()

        self.log_info('Executing the aws-ecr-image-scan pipe...')

        timeout = self.get_variable('TIMEOUT')
        timeout = timeout if timeout > 0 else 30*60*60  # if value is 0 set a max of about 30 minutes

        gf_response = self.get_findings()
        if gf_response.get('error') is not None:
            err = gf_response.get('error')
            if err == 'ScanNotFoundException':
                ss_response = self.start_scan(timeout)
                ss_err = ss_response.get('error')
                if ss_err is not None:
                    self.log_error(f'Failed to scan image: {ss_err}')
                    self.fail('Failed to get scan results, check output logs for information')
                else:
                    gf_response = self.get_findings()  # Get findings again
                    if gf_response.get('error') is not None:
                        self.fail(f'Failed to get scan results with error: {gf_response.get("error")}')
            else:
                self.fail(f'Failed to get scan results with error: {err}')

        self.process_findings(gf_response)
        self.exit()


if __name__ == '__main__':  # pragma: no cover
    pipe = ECRScan(pipe_metadata_file='/usr/bin/pipe.yml', schema=schema, logger=logger, check_for_newer_version=False)
    pipe.run()
