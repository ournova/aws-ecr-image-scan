from datetime import datetime

good_scan_results = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'status': 'COMPLETE',
        'description': 'Some test results'
    },
    'imageScanFindings': {
        'imageScanCompletedAt': datetime.now(),
        'vulnerabilitySourceUpdatedAt': datetime.now(),
        'findings': [],
        'findingSeverityCounts': {}
    }
}

in_progress_scan_results = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'status': 'IN_PROGRESS',
        'description': 'Some test results'
    },
    'imageScanFindings': {
        'imageScanCompletedAt': datetime.now(),
        'vulnerabilitySourceUpdatedAt': datetime.now(),
        'findings': [],
        'findingSeverityCounts': {}
    }
}

bad_scan_results = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'status': 'COMPLETE',
        'description': 'Some test results'
    },
    'imageScanFindings': {
        'imageScanCompletedAt': datetime.now(),
        'vulnerabilitySourceUpdatedAt': datetime.now(),
        'findings': [
            {
                'name': 'This is Informational',
                'description': 'A Informational Test',
                'uri': 'somestring',
                'severity': 'INFORMATIONAL',
                'attributes': [
                    {'key': 'package_version', 'value': '1.0.9.8.4'},
                    {'key': 'package_name', 'value': 'apt'},
                    {'key': 'CVSS2_VECTOR', 'value': 'AV:N/AC:M/Au:N/C:C/I:C/A:C'},
                    {'key': 'CVSS2_SCORE', 'value': '9.9'}
                ]
            },
            {
                'name': 'This is Low',
                'description': 'A Low Test',
                'uri': 'somestring',
                'severity': 'LOW',
                'attributes': [
                    {'key': 'package_version', 'value': '1.0.9.8.4'},
                    {'key': 'package_name', 'value': 'apt'},
                    {'key': 'CVSS2_VECTOR', 'value': 'AV:N/AC:M/Au:N/C:C/I:C/A:C'},
                    {'key': 'CVSS2_SCORE', 'value': '1.0'}
                ]
            },
            {
                'name': 'This is Medium',
                'description': 'A Medium Test',
                'uri': 'somestring',
                'severity': 'MEDIUM',
                'attributes': [
                    {'key': 'package_version', 'value': '1.0.9.8.4'},
                    {'key': 'package_name', 'value': 'apt'},
                    {'key': 'CVSS2_VECTOR', 'value': 'AV:N/AC:M/Au:N/C:C/I:C/A:C'},
                    {'key': 'CVSS2_SCORE', 'value': '3.1'}
                ]
            },
            {
                'name': 'This is High',
                'description': 'A High Test',
                'uri': 'somestring',
                'severity': 'HIGH',
                'attributes': [
                    {'key': 'package_version', 'value': '1.0.9.8.4'},
                    {'key': 'package_name', 'value': 'apt'},
                    {'key': 'CVSS2_VECTOR', 'value': 'AV:N/AC:M/Au:N/C:C/I:C/A:C'},
                    {'key': 'CVSS2_SCORE', 'value': '5.2'}
                ]
            },
            {
                'name': 'This is Critical',
                'description': 'A Critical Test',
                'uri': 'somestring',
                'severity': 'CRITICAL',
                'attributes': [
                    {'key': 'package_version', 'value': '1.0.9.8.4'},
                    {'key': 'package_name', 'value': 'apt'},
                    {'key': 'CVSS2_VECTOR', 'value': 'AV:N/AC:M/Au:N/C:C/I:C/A:C'},
                    {'key': 'CVSS2_SCORE', 'value': '7.3'}
                ]
            },
            {
                'name': 'This is Undefined',
                'description': 'A Undefined Test',
                'uri': 'somestring',
                'severity': 'undefined',
                'attributes': [
                    {'key': 'package_version', 'value': '1.0.9.8.4'},
                    {'key': 'package_name', 'value': 'apt'},
                    {'key': 'CVSS2_VECTOR', 'value': 'AV:N/AC:M/Au:N/C:C/I:C/A:C'},
                    {'key': 'CVSS2_SCORE', 'value': '9.4'}
                ]
            },
        ],
        'findingSeverityCounts': {
            'INFORMATIONAL': 1,
            'LOW': 1,
            'MEDIUM': 1,
            'HIGH': 1,
            'CRITICAL': 1,
            'UNDEFINED': 1,

        }
    },
    'nextToken': 'nextTokenString'
}

start_scan_complete = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'status': 'COMPLETE',
        'description': 'Some test results'
    }
}

start_scan_in_progress = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'status': 'IN_PROGRESS',
        'description': 'Some test results'
    }
}

start_scan_failed = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'status': 'FAILED',
        'description': 'Some test results'
    }
}

scan_not_found_error = {
    'Error': {
        'Code': 'ScanNotFoundException',
        'Message': 'TestScanNotFound'
    }
}

scan_limit_exceeded_error = {
    'Error': {
        'Code': 'LimitExceededException',
        'Message': 'TestScanNotFound'
    }
}

unsupported_image_error = {
    'registryId': 'test.resistry',
    'repositoryName': 'test',
    'imageId': {
        'imageDigest': '918374612897634891276348912764',
        'imageTag': 'test'
    },
    'imageScanStatus': {
        'description': "UnsupportedImageError: The operating system 'ubuntu' version '20.04' is not supported.",
        'status': 'FAILED'
        },
    }
